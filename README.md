## Description
The application is built without using any framework at all.
Everything is written using core Java.
I have tried to keep it as simple as possible without use of any heavy libraries.

## Requirements
 - Maven
 - Java 8

## Build
The application is a standalone jar. The following command will create the jar and execute unit tests.
```
mvn clean package
```
```
 java -jar target/rev-1.0-SNAPSHOT.jar 
```

## API testing
The service is instantiated on port 8000;

#### Displaying all accounts.
 The service creates 10 dummy accounts for testing purpose. You can create more accounts using the create account API.
```
curl -X GET http://localhost:8000/displayall 
```
#### Transferring from an account to another
The payload structure is a json:
```
curl -X POST http://localhost:8000/transfer -H 'Accept: */*'  -d '{"from":"REV3","to":"REV2","amount":"10"}'
```
#### Transactions
The below call returns all transactions in an account

```
 curl -X GET 'http://localhost:8000/transactions?account_number=REV1' -H 'Accept: */*'
```

Example output:
```
 [{"amount":100,"from":"REV1","to":"REV2","status":"OK"},{"amount":100,"from":"REV1","to":"REV2","status":"NO_BALANCE"},{"amount":100,"from":"REV1","to":"REV2","status":"NO_BALANCE"},{"amount":10,"from":"REV1","to":"REV2","status":"NO_BALANCE"}]
```


#### Creation of account
The API takes amount as request body parameter
```
curl -X POST http://localhost:8000/create_account -H 'Accept: */*' -d 4000
```