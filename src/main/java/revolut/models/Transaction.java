package revolut.models;

import org.json.JSONObject;

public class Transaction {
    Account from;
    Account to;
    int amount;
    TransactionStatus status;

    private Transaction() {

    }

    public Transaction(Account from, Account to, int amount, TransactionStatus status) {
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.status = status;
    }

    public Account getFrom() {
        return from;
    }

    public void setFrom(Account from) {
        this.from = from;
    }

    public Account getTo() {
        return to;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null || obj.getClass()!= this.getClass())
            return false;
        Transaction t = (Transaction) obj;

        return (t.from == this.from && t.to == this.to);
    }

    //A transaction between two accounts will always be same. Whichever direction the money flows.
    // Can be improved later.
    @Override
    public int hashCode() {
        return from.hashCode() + to.hashCode();
    }

    public void setTo(Account to) {
        this.to = to;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public JSONObject getJSON() {
        JSONObject obj = new JSONObject();
        obj.put("from", from.getAccountNumber());
        obj.put("to",to.getAccountNumber());
        obj.put("amount", amount);
        obj.put("status",status);
        return obj;
    }

    @Override
    public String toString() {
        return "{" +
                "\"from\":" + "\"" + from + "\"" +
                ", \"to\":" + "\"" + to + "\"" +
                ", \"amount\":" + "\"" + amount + "\"" +
                ", \"status:" + "\"" + status + "\"" +
                '}';
    }
}
