package revolut.models;

public enum TransactionStatus {
    OK,
    NO_BALANCE,
    INVALID_ACCOUNTS,
    PENDING
}
