package revolut.models;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Account {
    private int balance = 0;
    private String accountNumber;
    List<Transaction> transactions;

    private Account() {

    }

    public void addTransaction(Transaction transaction){
        synchronized (transaction) {
            transactions.add(transaction);
        }
    }
    public Account(String username, int balance) {
        this.accountNumber = username;
        this.balance = balance;
        transactions = new ArrayList<>();
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public void withdraw(int amount) {
        this.balance -= amount;
    }

    public void deposit(int amount) {
        this.balance += amount;
    }

    public int getBalance() {
        return this.balance;
    }

    public JSONObject getJSON(){
        JSONObject obj = new JSONObject();
        obj.put("Account Number",this.getAccountNumber());
        obj.put("Balance",this.getBalance());

        return obj;
    }

    @Override
    public String toString() {
        return "Account Number: " + this.accountNumber
                + " | Balance: " + this.balance
                + "\n";
    }
}