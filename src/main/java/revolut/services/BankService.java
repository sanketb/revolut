package revolut.services;

import revolut.models.Account;
import revolut.models.Transaction;
import revolut.models.TransactionStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BankService {
    public static final int MAX_ACCOUNT = 10;
    public static final int INITIAL_BALANCE = 100;
    public static final String PREFIX = "REV";
    private int autoId = 10000;

    private static BankService instance;
    private Map<String, Account> accounts = new HashMap();


    private BankService() {
        for (int i = 0; i < MAX_ACCOUNT; i++) {
            String number = PREFIX + i;
            accounts.put(number,new Account(number, INITIAL_BALANCE));
        }


    }
    synchronized static public BankService getInstance(){
        if(instance == null) {
            instance = new BankService();
        }
        return instance;
    }
    public Account create_account(int amount){
        String accountNumber = getNewId();
        Account account = new Account(accountNumber, amount);
        accounts.put(accountNumber, account);
        return account;
    }

    public String getNewId(){
        this.autoId++;
        return PREFIX + autoId;
    }

    public Account getAccount(String accountNumber){
        return this.accounts.get(accountNumber);
    }

    public TransactionStatus transfer(String from, String to, int amount) {
        if(!validate(from) || !validate(to)){
            return TransactionStatus.INVALID_ACCOUNTS;
        }

        Transaction transaction = new Transaction(accounts.get(from), accounts.get(to), amount, TransactionStatus.PENDING);

        // We have forced the transaction object to be identified as the same object for a pair of accounts. Check equals.
        synchronized (transaction) {
            if (amount <= accounts.get(from).getBalance()) {
                accounts.get(from).withdraw(amount);
                accounts.get(to).deposit(amount);
                transaction.setStatus(TransactionStatus.OK);
            } else {
                transaction.setStatus(TransactionStatus.NO_BALANCE);
            }
            accounts.get(from).addTransaction(transaction);
            accounts.get(to).addTransaction(transaction);
            return transaction.getStatus();
        }
    }

    public boolean validate(String accountNumber){
        return accounts.containsKey(accountNumber);
    }

    public List<Account> getAllAccounts(){

        return new ArrayList(accounts.values());
    }
}