package revolut.handlers;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import revolut.models.Account;
import revolut.models.Transaction;
import revolut.models.TransactionStatus;
import revolut.services.BankService;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class RequestHandler {
    HttpServer server;
    BankService bankService;

    public RequestHandler(int serverPort) throws IOException {
        server = HttpServer.create(new InetSocketAddress(serverPort), 0);
    }

    public void start() {

        bankService = BankService.getInstance();
        server.createContext("/displayall", getDisplayAllHttpHandler(bankService));

        server.createContext("/transfer", getTransferHttpHandler(bankService));
        server.createContext("/create_account", createAccountHandler(bankService));
        server.createContext("/transactions", getTransactions(bankService));

        server.setExecutor(null); // creates a default executor
        server.start();
    }

    HttpHandler getTransactions(BankService bank) {

        return he -> {
            Map<String, String> params = queryToMap(he.getRequestURI().getQuery());
            String accountNumber = params.get("account_number");
            Account account = bank.getAccount(accountNumber);
            String resp;
            int respCode;
            if (account == null) {
                resp = "{\"error\": \"No such account exists\"}";
                respCode = 400;
            } else {
                JSONArray array = new JSONArray();
                for (Transaction transaction : account.getTransactions()) {
                    array.put(transaction.getJSON());
                }
                resp = array.toString();
                respCode = 200;
            }
            he.sendResponseHeaders(respCode, resp.getBytes().length);
            OutputStream output = he.getResponseBody();
            output.write(resp.getBytes());
            output.flush();
            he.close();
        };


    }

    static HttpHandler createAccountHandler(BankService bank) {

        return he -> {
            InputStream is = he.getRequestBody();
            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, StandardCharsets.UTF_8);
            String body = writer.toString();
            int amount = Integer.parseInt(body);
            Account account = bank.create_account(amount);
            String resp = account.getJSON().toString();
            he.sendResponseHeaders(200, resp.getBytes().length);
            OutputStream output = he.getResponseBody();
            output.write(resp.getBytes());
            output.flush();
            he.close();
        };


    }

    HttpHandler getDisplayAllHttpHandler(BankService bank) {
        return exchange -> {

            JSONArray array = new JSONArray();
            for (Account acc : bank.getAllAccounts())
                array.put(acc.getJSON());
            String respText = array.toString();
            exchange.sendResponseHeaders(200, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
            exchange.close();
        };
    }

    HttpHandler getTransferHttpHandler(BankService bank) {
        return exchange -> {
            InputStream is = exchange.getRequestBody();
            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, StandardCharsets.UTF_8);
            String body = writer.toString();
            JSONObject obj = new JSONObject(body);

            String from = obj.getString("from");
            String to = obj.getString("to");
            String respText;
            int amount = 0;
            int respCode = 200;
            respText = "{message : Transfered from:" + from + ", to:" + to + "}";
            try {
                amount = Integer.parseInt(obj.getString("amount"));
            } catch (NumberFormatException e) {
                respText = "{error : Bad request}";
                respCode = 400;
            }
            TransactionStatus status = bank.transfer(from, to, amount);

            if (status.equals(TransactionStatus.INVALID_ACCOUNTS)) {
                respText = "{error :The account numbers you passed are wrong.}";
                respCode = 400;
            } else if (status.equals(TransactionStatus.NO_BALANCE)) {
                respText = "{error :Insufficient funds}";
                respCode = 400;
            }
            exchange.sendResponseHeaders(respCode, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
            exchange.close();
        };
    }


    private static Map<String, String> queryToMap(String query) {
        Map<String, String> result = new HashMap<String, String>();
        for (String param : query.split("&")) {
            String pair[] = param.split("=");
            if (pair.length > 1) {
                result.put(pair[0], pair[1]);
            } else {
                result.put(pair[0], "");
            }
        }
        return result;
    }
}
