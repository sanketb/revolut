package revolut;

import revolut.handlers.RequestHandler;

import java.io.IOException;
class Application {

    public static void main(String[] args) throws IOException {
        int serverPort = 8000;
        RequestHandler handler = new RequestHandler(serverPort);
        handler.start();
    }


}