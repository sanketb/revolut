package revolut.services;

import org.junit.Assert;
import org.junit.Test;
import revolut.models.Account;
import revolut.models.Transaction;
import revolut.models.TransactionStatus;

import java.util.List;

public class BankServiceTest {

    @Test
    public void testAccountCreation(){
        BankService service = BankService.getInstance();
        Account account = service.create_account(1000);
        Assert.assertNotNull(account);
        Assert.assertNotNull(account.getAccountNumber());
        Assert.assertEquals(account.getBalance(),1000);
        Assert.assertEquals(service.getAllAccounts().size(), BankService.MAX_ACCOUNT+1);
    }
    @Test
    public void testAccountTransfer(){
        BankService service = BankService.getInstance();
        service.transfer("REV1", "REV2", 10);
        Assert.assertEquals(service.getAccount("REV1").getBalance(),90);
        TransactionStatus status = service.transfer("REV1", "REV2", 10000);
        Assert.assertEquals(TransactionStatus.NO_BALANCE, status);

    }

    @Test
    public void testTransactions(){

            BankService service = BankService.getInstance();
            service.transfer("REV5", "REV6", 10);
            Assert.assertEquals(service.getAccount("REV5").getBalance(),90);
            TransactionStatus status = service.transfer("REV1", "REV2", 10000);
            Assert.assertEquals(TransactionStatus.NO_BALANCE, status);
            List<Transaction> transactionList = service.getAccount("REV5").getTransactions();
            Transaction transaction = transactionList.get(transactionList.size()-1);
            Assert.assertEquals(10,transaction.getAmount());


    }
}
